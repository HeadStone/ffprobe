package main

import (
	"encoding/json"
	"fmt"
	"os"
	"os/exec"
)

type Stream struct {
	Width  int `json:"width"`
	Height int `json:"height"`
	// Filename         string            `json:"filename"`
	// NBStreams        int               `json:"nb_streams"`
	// NBPrograms       int               `json:"nb_programs"`
	// FormatName       string            `json:"format_name"`
	// FormatLongName   string            `json:"format_long_name"`
	// StartTimeSeconds float64           `json:"start_time,string"`
	// DurationSeconds  float64           `json:"duration,string"`
	// Size             uint64            `json:"size,string"`
	// BitRate          uint64            `json:"bit_rate,string"`
	// ProbeScore       float64           `json:"probe_score"`
	// Tags             map[string]string `json:"tags"`
}

func main() {
	cmd := exec.Command("ffrobe", "-show_streams", "ad.mp4", "-print_format", "json")
	cmd.Stderr = os.Stderr
	r, err := cmd.StdoutPipe()

	if err != nil {
		return
	}
	err = cmd.Start()

	err = json.NewDecoder(r).Decode()

	if r != nil {
		fmt.Printf("test")
	}

}
